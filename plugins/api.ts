import { Plugin } from '@nuxt/types'
import apiModules from '@/api'

const apiPlugin: Plugin = (ctx, inject) => {
  const apiClient = ctx.$axios.create({
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    baseURL: '/api'
  })

  inject('api', apiModules(apiClient))
}

export default apiPlugin
