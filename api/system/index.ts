import type { NuxtAxiosInstance } from '@nuxtjs/axios'

export default (axios: NuxtAxiosInstance) => ({
  example () {
    return axios.get('example')
  }
})
