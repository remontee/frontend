import type { NuxtAxiosInstance } from '@nuxtjs/axios'
import system from './system'

export default (axios: NuxtAxiosInstance) => ({
  system: system(axios)
})
