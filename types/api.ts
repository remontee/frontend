import apiModules from '@/api'

type ApiPlugin = ReturnType<typeof apiModules>

declare module 'vue/types/vue' {
  interface Vue {
    $api: ApiPlugin;
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $api: ApiPlugin;
  }
  interface Context {
    $api: ApiPlugin;
  }
}

declare module 'vuex/types/index' {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Store<S> {
    $api: ApiPlugin;
  }
}
